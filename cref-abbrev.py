'''This script was written by Alessandra Gorla to create a super
abbreviated version of the crossref bibliography. Given
crossrefs-abbrev.bib, which already excludes month and address, this
script keeps only the acronyms of the conferences, which are assumed
to be before the ":".

Thus, "ICSE 2017: Proceedings of the International Conference ..."
will be reduced to "ICSE 2017".

If title and booktitle do not include any ":" the script keeps the
strings as they are.
'''

import re

with open('crossrefs-super-abbrev.bib', 'w') as out_file:
    with open('crossrefs-abbrev.bib') as cref_file:
        for l in cref_file.readlines():
            # this is the first line of title or booktitle, and we
            # need to remove what comes after the :
            if ("title =" in l):
                s = re.sub(':.*$', '",', l)
                # removing year
                s = ''.join([i for i in s if not i.isdigit()])
                out_file.write(s)
            # this is either another field that we should keep
            # entirely (e.g. year =) or the first line of the entry
            # @Proceedings
            elif ("=" in l or "@" in l):
                out_file.write(l)
            # this is the last line with only } and possible spaces
            elif (re.match(' *} *', l)):
                out_file.write(l+"\n")
            # if the line does not fall in previous cases it means
            # that it is a continuation of title/booktitle that should
            # be discarded
